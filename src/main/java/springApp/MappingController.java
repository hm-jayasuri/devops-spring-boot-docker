package springApp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MappingController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String root() {
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String index() {
        return "index.html";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo1")
    public String demoOne() {
        return "demopage_1.html";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/demo2")
    public String demoTwo() {
        return "demopage_2.html";
    }

}