package springApp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RESTController {

    @RequestMapping("/resources")
    public List<String> resources() {
        List<String> data = new ArrayList();
        data.add("Spring");
        data.add("Spring Boot");
        data.add("Maven");
        data.add("Ant");
        data.add("Gradle");
        data.add("Docker");
        data.add("IntelliJ");
        data.add("Eclipse");
        data.add("Microsoft Visual Studio Code");
        data.add("Git");
        return data;
    }

    @RequestMapping("/generalData")
    public Map generalData() {
        Map data = new HashMap<String, String>();
        data.put("Name", "Jan");
        data.put("Adresse", "Lothstr. 34");
        data.put("Studiengang", "Wirtschaftsinformatik");
        return data;
    }

}
