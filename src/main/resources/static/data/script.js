$(document).ready(function(){
    $('.twitter-control a').on('click',function(){
        $link = $(this);
        if(!$link.hasClass('active')){
            $($link).parent().find('.active').removeClass('active');
            $link.addClass('active');
            var feed = $link.attr('data-feed');

            $('.twitter_wrapper').each(function(){
                var $this = $(this);
                if(!$this.hasClass('hidden')){
                    if($this.attr('id') != feed){
                        $this.addClass('hidden');
                    }
                }else{
                    if($this.attr('id') == feed){
                        $this.removeClass('hidden');
                    }
                }
            });
        }
    });
});